"use strict";
(function (root, factory) { "function"==typeof define&&define.amd?define(factory):"object"==typeof module&&module.exports&&(module.exports=factory(!0)); }(this, function () {
    function resolve(e){if(arguments.length>1){for(var r=[],n=0;n<arguments.length;n++)r.push(resolve(arguments[n]));e=r.join("/")}else for(var a=e.match(/\{[^${]*}/g);a;){for(var g,t,n=0;n<a.length;n++)t=a[n],g=t.replace("{","").replace("}",""),e=e.replace(new RegExp(t,"g"),tokens[g]);a=e.match(/\{[^${]*}/g)}return e=e.replace(new RegExp("//","g"),"/")};

    var IS_NODE = typeof window == "undefined";

    var tokens = {
        root : IS_NODE ? __dirname : "/",
        npm : "{root}/node_modules",
        src : "{root}/src",
        res : "{root}/res"
    };

    var config = {
        name : "main",
        baseUrl : resolve("{src}"),
        shim : {
            screenfull : {
                exports : "screenfull"
            }
        },
        paths : {
            "res"                           : resolve("{res}"),
            "package"                       : resolve("{root}", "package.json"),
            "file"                          : resolve("{root}" , "vendor/requirejs.text"),
            "postal"                    	: resolve("{npm}" , "postal/postal"),
            "superagent"                    : resolve("{npm}" , "superagent/superagent"),
            "base"                          : resolve("{npm}" , "basejs/base"),
            "laya"                          : resolve("{npm}" , "laya/src/Laya"),
            "todo"                          : resolve("{npm}" , "todojs/todo"),
            "sender"                        : resolve("{npm}" , "sender/sender"),
            "centrifuge"                    : resolve("{npm}" , "centrifuge/centrifuge.min"),
            "unicycle"                      : resolve("{npm}" , "unicycle/unicycle"),
            "tweener"                       : resolve("{npm}" , "tweener/tweener"),
            "strukt"                        : resolve("{npm}" , "strukt/strukt"),
            "less"                          : resolve("{npm}" , "less/dist/less"),
            "vue"                           : resolve("{npm}" , "vue/dist/vue"),
            // "parcel"                        : resolve("{src}" , "App/Parcel"),
        },
        urlArgs : "bust=" + (+new Date()),
        waitSeconds : 720,
        generateSourceMaps : true,
        preserveLicenseComments : false,
        out : "dist/main.js",
        findNestedDependencies : true,
        catchError : { define : true },
        useStrict : true,
        optimize : "uglify2",
        uglify2 : {
            output: {
                beautify: false,
                preamble : "/*Baloven*/"
            },
            compress : {
                drop_console : true,
                keep_infinity : true,
                passes : 4,
                sequences : true,
                unsafe_math : true,
                dead_code: true,
                conditionals: true,
                booleans: true,
                unused: true,
                if_return: true,
                join_vars: true,
            },
        }
    };

    return config;
}));
