// ==UserScript==
// @namespace baloven
// @match https://www.bitfinex.com/trading
// @match https://www.bitfinex.com/t/*
// @grant none
// @author sanyabeast<a.gvrnsk@gmail.com>
// @name Baloven
// @version 0.2.88-a

// ==/UserScript==

(function() {
/*Baloven*/
"use strict";var ScriptRunner=function(){this.extra.loopList(this.loaders,function(t,e,n){n[e]=t.bind(this)},this),this.extra.loopList(this.dom,function(t,e,n){n[e]=t.bind(this)},this),this.extra.loopList(this.extra,function(t,e,n){n[e]=t.bind(this)},this)};ScriptRunner.prototype={loaders:{js:function(t,e){var n=this;this.loaders.file(t,function(t){n.extra.evalInContext(this.responseText,window.top),e&&e.apply(this,arguments)})},html:function(t,e){var n=this;this.loaders.file(t,function(t){document.body.appendChild(n.dom.html2DOM(this.responseText)),e&&e.apply(this,arguments)})},css:function(t,e){this.loaders.file(t,function(t){var n=document.createElement("style");n.type="text/css",n.innerHTML=this.responseText,document.getElementsByTagName("head")[0].appendChild(n),e&&e.apply(this,arguments)})},file:function(t,e){var n=new XMLHttpRequest;n.onload=e,n.open("get",t,!0),n.send()}},dom:{processScripts:function(t){for(var e,n=t.querySelectorAll("script"),i=0;i<n.length;i++)if((e=n[i]).hasAttribute("data-type"))switch(e.getAttribute("data-type")){case"element-script":this.extra.evalInContext(e.innerText,e.parentNode)}},html2DOM:function(t){var e=document.createElement("div");e.innerHTML=t;for(var n=e.children,i=document.createDocumentFragment();n.length;){var r=e.removeChild(n[0]);this.dom.processScripts(r),i.appendChild(r)}return i},addScriptElement:function(t,e){var n=document.createElement("script");n.onload=function(){alert("Script loaded and ready")},this.extra.loopList(e,function(t,e){n.setAttribute(["data",e].join("-"),t)},this),n.src=t,document.getElementsByTagName("head")[0].appendChild(n)}},extra:{loopList:function(t,e,n){for(var i in t)e.call(n,t[i],i,t)},loop:function(t,e,n){for(var i=0,r=t.length;i<r;i++)e.call(n,t[i],i,t)},evalInContext:function(code,context){(function(){eval(code)}).call(context)},wait:function(t,e,n){n=n||1e3,t()?e():setTimeout(function(){this.extra.wait(t,e,n)},n)}},loadApp:function(t){if(!t.readyChecker||t.readyChecker&&t.readyChecker()){var e=t.modules;this.extra.loop(e,function(t){var e=t[0],n=t[1];this.loaders[e]&&this.loaders[e](n)},this)}else setTimeout(function(){this.loadApp(t)}.bind(this),t.readyCheckTimeout||1e3)}};var runner=window.runner=new ScriptRunner;runner.loaders.js("https://bitbucket.org/sanyabeast/cashwalker-dist/raw/master/vendor/r.js",function(){require(["https://bitbucket.org/sanyabeast/cashwalker-dist/raw/master/dist/main.js"],function(t){})});
})();