"use strict";
var ScriptRunner = function(){
	this.extra.loopList(this.loaders, function(method, name, list){
		list[name] = method.bind(this);
	}, this);

	this.extra.loopList(this.dom, function(method, name, list){
		list[name] = method.bind(this);
	}, this);

	this.extra.loopList(this.extra, function(method, name, list){
		list[name] = method.bind(this);
	}, this);
};

ScriptRunner.prototype = {
	loaders : {
		js : function(url, callback){
			var _this = this;
	    	this.loaders.file(url, function(data){
	    		console.log("JS Loaded:", url);
	    		_this.extra.evalInContext(this.responseText, window.top);
	    		if (callback) callback.apply(this, arguments);
	    	});
	    },

	    html : function(url, callback){
	    	var _this = this;
	    	this.loaders.file(url, function(data){
	    		console.log("HTMl Loaded:", url);
	    		document.body.appendChild(_this.dom.html2DOM(this.responseText));
	    		if (callback) callback.apply(this, arguments);
	    	});
	    },

	    css : function(url, callback){
	    	var _this = this;
	    	this.loaders.file(url, function(data){
	    		console.log("CSS Loaded:", url);
	    		var style = document.createElement("style");
				style.type = "text/css";
				style.innerHTML = this.responseText;
				document.getElementsByTagName("head")[0].appendChild(style);
				if (callback) callback.apply(this, arguments);
	    	});
	    },

	    file : function(url, callback){
	    	var req = new XMLHttpRequest();
			req.onload = callback;
			req.open("get", url, true);
			req.send();
	    }
	},
	dom : {
		processScripts : function(dom){
	    	var scripts = dom.querySelectorAll("script");

	    	for (var a = 0, type, script; a < scripts.length; a++){
	    		script = scripts[a];

	    		if (script.hasAttribute("data-type")){
	    			type = script.getAttribute("data-type");
	    			
	    			switch(type){
	    				case "element-script":
	    					this.extra.evalInContext(script.innerText, script.parentNode);
	    				break;
	    			}
	    		}
	    	}

	    },
	    html2DOM : function(html){
	    	var el = document.createElement("div");
	    	el.innerHTML = html;

	    	var childNodes = el.children;
	    	var result = document.createDocumentFragment();

	    	while(childNodes.length){
	    		var child = el.removeChild(childNodes[0]);
	    		this.dom.processScripts(child);
	    		result.appendChild(child);
	    	}

	    	return result;
	    },
	    addScriptElement : function(url, attrs){
	    	var script = document.createElement("script");
			script.onload = function() {
			  alert("Script loaded and ready");
			};


			this.extra.loopList(attrs, function(value, name){
				script.setAttribute(["data", name].join("-"), value);
			}, this);

			script.src = url;
			document.getElementsByTagName("head")[0].appendChild(script);
	    }
	},
	extra : {
		loopList : function(list, cb, ctx){
			for (var k in list){
				cb.call(ctx, list[k], k, list);
			}
		},
		loop : function(arr, cb, ctx){
			for (var a = 0, l = arr.length; a < l; a++){
				cb.call(ctx, arr[a], a, arr);
			}
		},
		evalInContext : function(code, context){
	 		(function(){
	 			eval(code);
	 		}.call(context));
	    },
	    wait : function(checker, callback, freq){
	    	freq = freq || 1000;

	    	if (checker()){
	    		callback();
	    	} else {
	    		setTimeout(function(){
	    			this.extra.wait(checker, callback, freq);
	    		}, freq);
	    	}

	    }
	},

	loadApp : function(params){
		if (!params.readyChecker || (params.readyChecker && params.readyChecker())){
			var modules = params.modules;

    		this.extra.loop(modules, function(data){
    			var type = data[0];
    			var src = data[1];

    			if (this.loaders[type]){
    				this.loaders[type](src);
    			}
    		}, this);
		} else {
			setTimeout(function(){
				this.loadApp(params);
			}.bind(this), params.readyCheckTimeout || 1000);
		}
	}
};

/*------------------------------------------------------------------------*/
/*The story begins...                                                     */
/*------------------------------------------------------------------------*/

var runner =  window.runner = new ScriptRunner();

runner.loaders.js("https://bitbucket.org/sanyabeast/cashwalker-dist/raw/master/vendor/r.js", function(){
	require(["https://bitbucket.org/sanyabeast/cashwalker-dist/raw/master/dist/main.js"], function(main){
		
	});
});